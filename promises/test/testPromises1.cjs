const board = require("../../data/boards_1.json");

const getBoardInfo = require("../promises1.cjs");

function getBoard(boardId) {
  const index = Math.floor(Math.random() * board.length);
  boardId = boardId || board[index].id;
  getBoardInfo(boardId)
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

getBoard();
