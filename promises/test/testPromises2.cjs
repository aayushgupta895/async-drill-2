const board = require("../../data/boards_1.json");

const getBoardList = require("../promises2.cjs");

function getListOfBoard() {
  const index = Math.floor(Math.random() * board.length);
  getBoardList(board[index].id)
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

getListOfBoard();
