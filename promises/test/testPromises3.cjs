const lists = require("../../data/lists_1.json");

const getCards = require("../promises3.cjs");

function getCardInfo() {
  const keys = Object.keys(lists);
  const index = Math.floor(Math.random() * Object.keys(keys).length);
  console.log(`the id we are searching for ${lists[keys[index]][index].id}`);
  getCards(lists[keys[index]][index].id)
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

getCardInfo();
