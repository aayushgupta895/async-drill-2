/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getBoardInfo = require("./promises1.cjs");
const getBoardList = require("./promises2.cjs");
const getCards = require("./promises3.cjs");

function getInfo() {
  getBoardInfo("mcu453ed")
    .then((data) => {
      console.log(data);
      getBoardList(data.id)
        .then((data) => {
          console.log(data);
          const res = data.list.filter((res) => {
            return res.name == "Mind" || res.name == "Space";
          });
          const result = {};
          let finishedExecuting = 0;
          res.forEach((element) => {
            getCards(element.id)
              .then((data) => {
                result[element.id] = data;
                finishedExecuting++;
                if (finishedExecuting == res.length) {
                  console.log(result);
                }
              })
              .catch((err) => {
                console.log(err);
              });
          });
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports = getInfo;
