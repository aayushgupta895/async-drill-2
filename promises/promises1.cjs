/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boards = require("../data/boards_1.json");

function getBoardInfo(boardId) {
  return new Promise((resolve, reject) => {
    if (!boardId) {
      reject(new Error(`boardId can not be null`));
      return;
    }
    const timer = Math.floor(Math.random() * 2) + 2; // will get only 2 or 3
    console.log(`wait for ${timer} sec`);
    return setTimeout(() => {
      const boardInfo =
        boards.find((board) => {
          return board.id == boardId;
        }) || [];
      resolve(boardInfo);
    }, timer * 1000);
  });
}

module.exports = getBoardInfo;
