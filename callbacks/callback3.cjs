/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const cards = require('../data/cards_1.json');

function getCards(listId, callback) {
  if (!listId) {
    callback(new Error(`listId can not be null`));
    return;
  }
  const timer = Math.floor(Math.random() * 2) + 2; // will get only 2 or 3
  console.log(`wait for ${timer} sec for id ${listId}`);
  setTimeout(() => {
    if(Object.keys(cards).includes(listId)){ 
        callback(null, cards[listId]);
    }else{
        callback(`the card of given list id ${listId} is not present`);
    }
  }, timer * 1000);
}

module.exports = getCards;