/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const boards = require("../data/boards_1.json");
const lists = require('../data/lists_1.json');

function getBoardList(boardId, callback) {
  if (!boardId) {
    callback(new Error(`boardId can not be null`));
    return;
  }
  const timer = Math.floor(Math.random() * 2) + 2; // will get only 2 or 3
  console.log(`wait for ${timer} sec`);
  setTimeout(() => {
    const boardInfo =
      boards.find((board) => {
        return board.id == boardId;
      });
      if(!boardInfo){
        callback('cannot get list of given boardId');
        return;
      }else{
        if(Object.keys(lists).includes(boardInfo.id)){
          boardInfo.list = lists[boardInfo.id];
        }else{
          boardInfo.list = [];
        }
      }
    callback(null, boardInfo);
  }, timer * 1000);
}

module.exports = getBoardList;
