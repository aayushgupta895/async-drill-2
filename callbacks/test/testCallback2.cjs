const board = require("../../data/boards_1.json");

const getBoardList = require("../callback2.cjs");

function getListOfBoard() {
  const index = Math.floor(Math.random() * board.length);
  getBoardList(board[index].id, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
    }
  });
}

getListOfBoard();
